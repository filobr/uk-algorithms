const fs = require("fs");
const path = require("path");

(function init() {
  const users = JSON.parse(
    fs.readFileSync(path.resolve(__dirname, "../data/users.json"), "utf-8")
  );
  const mobileDevices = JSON.parse(
    fs.readFileSync(
      path.resolve(__dirname, "../data/mobile_devices.json"),
      "utf-8"
    )
  );
  const iotDevices = JSON.parse(
    fs.readFileSync(
      path.resolve(__dirname, "../data/iot_devices.json"),
      "utf-8"
    )
  );

  console.log(new Date().toISOString());
  console.log(count(users, mobileDevices, iotDevices));
  console.log(new Date().toISOString());
})();

function count(users, mobileDevices, iotDevices) {
  const iotDevicesByUser = users.map(user => {
    const userName = user.name.replace(/ .*/, "");
    const userMobileDevices = mobileDevices
      .filter(device => device.user.includes(user.id))
      .map(element => element.id);
    const userIotDevices = iotDevices.filter(device =>
      userMobileDevices.includes(device.mobile)
    );
    return { name: userName, number: userIotDevices.length };
  });

  const iotDevicesByName = iotDevicesByUser.reduce((acc, curr) => {
    const objInAcc = acc.find(o => o.name === curr.name);
    if (objInAcc) objInAcc.number += curr.number;
    else acc.push(curr);
    return acc;
  }, []);

  const display = iotDevicesByName.map(
    ({ name, number }) => name + " => " + number
  );

  return display;
}
